/* 
 * To This is the Main script for google maps, querying and parsing.
 * Using SlimerJs as core.
 */


 // Setting Global Variables

phantom.casperPath = 'C:/Users/DELL/Documents/NetBeansProjects/GoogleMapsSlimerJs/n1k0-casperjs-4f105a9/';
phantom.injectJs(phantom.casperPath + '\\bin\\bootstrap.js');


var resources=0;
var casper = require('casper').create({
    verbose: true,
    logLevel: "debug",
    waitTimeout:30000,    
    onResourceReceived: function () {
        //console.log('ON RESOURCE RECEIVED');
        controlFlags.resources++;
    },         
     viewportSize: {
        width: 700,
        height: 530
    },
            
    clientScripts: ["jquery-1.11.2.js"]
});


var fs = require('fs');

//Global Variables
var controlFlags = {
    "pagination" : 5,
    "actual_index" : 0,
    "resources": 0,
    "info_logs" : 'logs/info.txt',
    "error_logs" : 'logs/error.txt'
};

/*Labels for the Item Result Data */
var tags = {
    "index" : 0,
    "dom_item_title" : 'h1[class="cards-entity-title cards-strong cards-text-truncate-and-wrap"]',
    "dom_item_phone" : 'div[class=cards-expanded] div[class=cards-entity-phone]',
    "dom_item_url" : 'span[class=cards-social-rating-row-child] a',
    "dom_item_website" : 'div[class=cards-entity-url] a[class=cards-text-truncate] span',
    "dom_item_address" : 'div[class="cards-entity-address cards-strong"]',
    "dom_item_score" : 'div[class=cards-rating-div] span[class=cards-rating-score]',
    "dom_item_reviews" : 'span[class="cards-oneline-list cards-social-rating-reviews"] a',
    "not_found" : "Not_Found",
    "dom_click_pagination" : 'span[class=cards-categorical-pagination-button-right]',
    "dom_click_item_search_part_a" : 'div[class=cards-categorical-list] div[class="cards-categorical-list-scrollbox jfk-scrollbar jfk-scrollbar-borderless"] div[data-list-index="',
    "dom_click_item_search_part_b" : '"] div[class=cards-categorical-list-item]',
    "dom_click_back_to_results" : 'div[class=cards-categorical-listlink] div[class=cards-categorical-list-context-card]'
};
/**
 * Item= Google Maps Business.
 * Click= left click mouse Event.
 */

var TAKE_LAST_CAPTURE=true; /*Testing development purposes */ 
var RESPONSE={ /* JSON Object for Response*/
    items:[]
};

//--------------------------------------------------------------------------------------------------------
/*var country=casper.cli.get('country');
var country=casper.cli.get('search');
var country=casper.cli.get('location');

casper.echo(country);
casper.echo(search);
casper.echo(location);*/

casper.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36');

var test_url='https://www.google.com/maps/search/restaurant+new+york/@40.7398325,-73.9845505,13z/data=!3m1!4b1';

//-------------------------------------------------------------------------------------------------
var report=function(start){
    var end=Date.now();
    console.log("---------------------------------------------");
    console.log("Running Time: "+ (end - start) / 1000 + " Seconds.");
    console.log("---------------------------------------------");
};
//--------------------------------------------------------------------------------------------

var logToFile = function (log_string, info) {
    var stream = null;
    if (info) {
        stream = fs.open(controlFlags.info_logs, 'a+');
    } else if (!info) {
        stream = fs.open(controlFlags.error_logs, 'a+');
    }
    
    if (stream){
        stream.write(log_string+'\n');
        stream.close();
    }
};


var matchResultsTags = function () {      
    return !casper.exists(tags.dom_click_item_search_part_a + 10 + tags.dom_click_item_search_part_b) && !casper.exists(tags.dom_click_item_search_part_a + 9 + tags.dom_click_item_search_part_b) && !casper.exists(tags.dom_click_item_search_part_a + 8 + tags.dom_click_item_search_part_b) && !casper.visible(tags.dom_click_item_search_part_a + 10 + tags.dom_click_item_search_part_b) && !casper.visible(tags.dom_click_item_search_part_a + 1 + tags.dom_click_item_search_part_b);
};

var waitMainResults = function () {
    casper.wait(100, function () {
        
        if (matchResultsTags()) {
            this.emit('waitMainResults');           
        } else {
            this.echo('Main Results Loaded'); 
        }                
    });
};

var backToResults = function () {
    casper.click(tags.dom_click_back_to_results);
    this.emit('waitMainResults');
};

casper.on('waitMainResults', function () {
    waitMainResults();
});

casper.on('clickItem', function (index) {
    this.click(tags.dom_click_item_search_part_a + index + tags.dom_click_item_search_part_b);  
    this.emit('waitItem', index);
});

casper.on('paginate', function () {
    try {
        this.click(tags.dom_click_pagination);
    } catch(e) {console.log(e);}
});

casper.on('backToResults', function () {
    this.click(tags.dom_click_back_to_results);
    this.emit('waitMainResults');
  
});

casper.on('waitItem', function (index) {
    this.wait(1000, function () {

    var check='div[class="cards-entity cards-padding"]';
        if (this.exists(check) && this.visible(check)) {           
             var data = {};
            
            controlFlags.actual_index++;
            tags.index = index;
            
            data = this.evaluate(function(tags) {
                var raw = {};

                try {
                    raw.title = $(tags.dom_item_title).first().text();
                } catch (e) {
                    console.log(e);
                    raw.title = tags.not_found;
                }
                try {
                    raw.phone = $(tags.dom_item_phone).text();
                } catch (e) {
                    console.log(e);
                    raw.phone = tags.not_found;
                }
                try {
                    raw.url = $(tags.dom_item_url).attr('href');
                } catch (e) {
                    console.log(e);
                    raw.url = tags.not_found;
                }
                try {
                    raw.website = $(tags.dom_item_website).text();
                } catch (e) {
                    console.log(e);
                    raw.website = tags.not_found;
                }
                try {
                    raw.address = $(tags.dom_item_address).text();
                } catch (e) {
                    console.log(e);
                    raw.address = tags.not_found;
                }
                try {
                    raw.score = $(tags.dom_item_score)[tags.index - 1].textContent;
                } catch (e) {
                    console.log(e);
                    raw.score = tags.not_found;
                }
                try {
                    raw.reviewCount = $(tags.dom_item_reviews).text().replace(/\D/g, '');
                } catch (e) {
                    raw.reviewCount = tags.not_found;
                }

                return raw;
            }, {tags:tags} );           
            
            
            this.echo(JSON.stringify(data));            
            RESPONSE.items.push(data);
            
            try {
                logToFile(controlFlags.actual_index+ ': '+JSON.stringify(data), true);
            } catch(e) {
                console.log(e);
            }          
            
            this.emit('backToResults');
            //backToResults();
        } else {
            console.log('Waiting Item');
            this.emit('waitItem', index);
        }
    });
});

casper.on('load', function(paginate, index) {
    
    casper.wait(100, function() {
        console.log('Resources: ' + controlFlags.resources);        
        this.echo('Paginate: ' + this.visible(tags.dom_click_pagination));       
        
        if (!this.exists(tags.dom_click_item_search_part_a + 10 + tags.dom_click_item_search_part_b) && !this.exists(tags.dom_click_item_search_part_a + 9 + tags.dom_click_item_search_part_b) && !this.exists(tags.dom_click_item_search_part_a + 8 + tags.dom_click_item_search_part_b) && !this.visible(tags.dom_click_item_search_part_a + 10 + tags.dom_click_item_search_part_b) && !this.visible(tags.dom_click_item_search_part_a + 1 + tags.dom_click_item_search_part_b)) {
            this.emit('load', paginate, index);
        } else {
              if (!paginate)
                  this.capture('shoots/maps '+index+' .PNG');
            if (paginate) {
                console.log('+++++Calling Pagination+++++');
                this.emit('paginate');
            }
        }
    });
});

/* Starting the script and collect the current time in milliseconds */
casper.start().then(function(){
    start=Date.now();
});

casper.thenOpen('https://www.google.co.uk/preferences?fg=1#location',function(){
        this.echo(this.getCurrentUrl());  
        this.sendKeys('input[name=luul]','London');
        //this.click('div[id="form-buttons"] div[class="goog-inline-block jfk-button jfk-button-action"]');
        this.clickLabel('Save','div');
       
        
        //}, location);
        
});

casper.then(function(){
    var cookies = JSON.stringify(phantom.cookies);
    fs.write("cookies.txt", cookies, 644);
    
});

casper.then(function() {
    try {

        var data = fs.read("cookies.txt");
        var cookies = JSON.parse(data);

        for (var i = 0; i < cookies.length; i++) {
            phantom.addCookie(cookies[i]);
        }

    } catch (e) {
        console.log('Error Setting Cookies ' + e);
        logToFile('Error Setting Cookies ' + e, false);
    }
});

/* Open the Google Maps Url*/
casper.thenOpen(test_url,function(){
        this.echo(this.getCurrentUrl());        
});




//-------------------------------------------------------------------------
var pos=1;
casper.repeat(controlFlags.pagination, function () {
     
    casper.then(function () {
        try {
           this.emit('load', false, pos);
        } catch (e) {
            console.log( e );
            logToFile('Error Loading Main Results.' , false);
            logToFile(e , false);
        }
    });

    casper.then(function () {
        try {
            this.emit('clickItem', 1);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+1 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
            this.emit('clickItem', 2);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+2 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
           this.emit('clickItem', 3);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+3 , false);
            logToFile(e , false);
        }
    });
   
    casper.then(function () {
        try {
            this.emit('clickItem', 4);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+4 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
            this.emit('clickItem', 5);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+5 , false);
            logToFile(e , false);
        }
    });
   
    casper.then(function () {
        try {
            this.emit('clickItem', 6);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+6 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
            this.emit('clickItem', 7);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+7 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
            this.emit('clickItem', 8);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+8 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
        this.emit('clickItem', 9);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+9 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
           this.emit('clickItem', 10);
        } catch (e) {
            console.log( e );
            logToFile('Error Clicking Item: '+10 , false);
            logToFile(e , false);
        }
    });
    
    casper.then(function () {
        try {
            this.emit('load', true, pos);
            pos++;
        } catch (e) {
            console.log( e );
            logToFile('Error Paginating '+pos , false);
            logToFile(e , false);
        }  
    });

   
  
});


//--------------------------------------------------------------------------------------------------
/* Here just take the last capture, in case needed, just for development or testing purposes */
casper.then(function() {
    if (TAKE_LAST_CAPTURE) {
        casper.wait(100, function() {
            console.log(resources);
            this.capture('shoots/maps.png');
        });
    }//end if
});

//------------------------------------------------------------------------------------------------
/* Here we will print the JSON response and the processing time report.*/
casper.then(function(){
    try{
      report(start);
    } catch (e) {
       console.log(e.toString());
    }  
});

//-----------------------------------------------------------------------------
casper.run(function() {   
    this.exit();
});
